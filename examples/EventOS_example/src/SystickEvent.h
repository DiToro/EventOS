/*
 * SystickEvent.h
 *
 *  Created on: 01/10/2014
 *      Author: Samuel
 */

#ifndef SYSTICKEVENT_H_
#define SYSTICKEVENT_H_

void SystickEvent_new(void);
void SystickEvent_delete(void);
uint32_t getMsTicks(void);

#endif /* SYSTICKEVENT_H_ */
